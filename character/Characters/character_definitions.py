Races = {
    'Human': {
        'strength': 6,
        'agility': 6,
        'intelligence': 6,
        'stamina': 6,
        'charisma': 6,
        'ability': #
    },
    'Hydrosa': { ##Water based alien
        'strength': 3,
        'agility': 8,
        'intelligence': 7,
        'stamina': 7,
        'charisma': 5,
        'ability': #
    },
    'Shrubnola': { #Tree based alien
        'strength': 7,
        'agility': 7,
        'intelligence': 4,
        'stamina': 5,
        'charisma': 7,
        'ability': #
    },
    'Android': {
        'strength': 7,
        'agility': 4,
        'intelligence': 9,
        'stamina': 7,
        'charisma': 3,
        'ability': #
    }
}

Classes = {
    'Trooper': {
        'strength_modifier': 2,
        'agility_modifier': 2,
        'intelligence_modifier':-1,
        'stamina_modifier': 1
    },
    'Pilot': {
        'strength_modifier': -1,
        'agility_modifier': 3,
        'intelligence_modifier': 2,
        'stamina_modifier':-1
    },
    'Spy': {
        'strength_modifier': -1,
        'agility_modifier': 2,
        'intelligence_modifier': 3,
        'stamina_modifier': -1
    }
}
