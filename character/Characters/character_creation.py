from Characters.character_definitions import Races, Classes
from ..models import Character

def create_character(name, race_index, class_index):
    ## Get Selected Race and Class
    selected_race = list(Races.keys())[race_index - 1]
    selected_class = list(Classes.keys())[class_index -1]
    ## Get the attribute modifiers for selected race
    race_attributes = Races[selected_race]
    strength = race_attributes['strength']
    agility = race_attributes['agility']
    intelligence = race_attributes['intelligence']
    stamina = race_attributes['stamina']
    ## Get the attribute modifiers for selected class
    class_attributes = Classes[selected_class]
    strength_modifier = class_attributes['strength_modifier']
    agility_modifier= class_attributes['agility_modifier']
    intelligence_modifier = class_attributes['intelligence_modifier']
    stamina_modifier = class_attributes['stamina_modifier']
    ## Apply modifiers to base attributes
    strength += strength_modifier
    agility += agility_modifier
    intelligence += intelligence_modifier
    stamina += stamina_modifier

    ## Create Character object
    character = Character(
        name=name,
        race=selected_race,
        character_class=selected_class,
        strength=strength,
        agility=agility,
        intelligence=intelligence
        stamina=stamina
    )

    character.save()

    return character
